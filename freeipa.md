# FreeIPA

Referencia: [FreeIPA](https://www.freeipa.org/page/Main_Page)
Notas del curso: [LPIC3 Notes](https://hackmd.io/@gaioso/lpic3_notes)

## Fichero Vagrantfile

```
Vagrant.configure("2") do |config|

         config.vm.define "centos1" do |centos1|
           centos1.vm.box = "centos/7"
           centos1.vm.network "private_network", ip: "192.168.33.10", virtualbox__intnet: "interna"
           centos1.vm.hostname = "centos1"
         end

         config.vm.define "centos2" do |centos2|
           centos2.vm.box = "centos/7"
           centos2.vm.network "private_network", ip: "192.168.33.11", virtualbox__intnet: "interna"
           centos2.vm.hostname = "centos2"
         end

         config.vm.define "centos3" do |centos3|
           centos3.vm.box = "centos/7"
           centos3.vm.network "private_network", ip: "192.168.33.12", virtualbox__intnet: "interna"
           centos3.vm.hostname = "centos3"
         end
end
```

En la máquina *centos3* se ha añadido un gestor de escritorio *xfce* para poder acceder a diferentes recursos a través de un navegador web. Esta máquina también actuará como cliente *NFS*. A mayores, en la máquina *centos3* se ha incrementado la memoria RAM hasta un total de 2GB.

> **AVISO** Para evitar problemas con la instalación del servidor de *FreeIPA* debemos aumentar la memoria RAM de *centos1* también a 2GB. En la sesión grabada no se hace desde el principio y no es posible finalizar la instalación.

> Para instalar *xfce* en *centos3* ejecutamos los siguientes comandos:
> `yum -y groups install "Server with GUI"
> `yum install epel-release -y`
> `yum --enablerepo=epel -y groups install "Xfce"`
> `startx`

Resumiendo:

* *centos1* será nuestro «servidor KDC»
* *centos2* el servidor NFS
* *centos3* el equipo cliente

En la máquina *centos* vamos a realizar los siguientes cambios:

1. En el fichero `/etc/hosts` modificamos las siguientes líneas:
	
	192.168.33.10	server.lpic.lan server
	127.0.0.1	server

> **AVISO** Aunque al inicio de la sesión se configura el fichero anterior con estos valores, para evitar problemas de instalación se debe dejar solo la primera línea en el fichero [192.168.33.10  server.lpic.lan server] 

2. Modificamos el *hostname* de la máquina:

	# hostname server
 
> Este cambio en el nombre del *host* no es permanente si se reinicia la máquina.

3. Añadimos la siguiente línea al fichero `/etc/resolv.conf`:

	search lpic.lan

### Instalación de FreeIPA

Procedemos con la instalación de *freeipa* en *centos1*:

	# yum install rng-tools -y
	# rngd
	# yum install ipa-server -y
	[...]
	# ipa-server-install
	Do you want to configure integrated DNS (BIND)? [no]: <INTRO>
	Server host name [server]: server.lpic.lan
	Please provide a realm name [LPIC.LAN]: <INTRO>
	Directory Manager password: <contraseña>
	Password (confirm): <contraseña>
	IPA admin password: <contraseña>
	Password (confirm): <contraseña>
	
	Continue to configure the system with these values? [no]: yes

Mientras se ejecuta la instalación en *centos1*, nos movemos a *centos2* para realizar las siguientes tareas:

1. Creamos el directorio para compartir a través de NFS:

	# mkdir /shared
	# chmod 777 /shared

2. Creamos la configuración de *exports* en NFS:

	# vi /etc/exports
	/shared *(rw)	
	
	# exportfs -r
	# systemctl start nfs-server
	# showmount -e localhost
	Export list for localhost:
	/shared *		# /shared *(sec=krb5,rw)

> La segunda opción es necesaria para el montaje desde el cliente cuando tengamos el *ticket* de Kerberos.

3. Actualizamos la información del fichero `/etc/hosts`:

	192.168.33.10	server.lpic.lan server
	127.0.0.1	nfserver nfserver.lpic.lan

4. Cambiamos el *hostname* de la máquina:

	# hostname nfserver

5. Añadimos la siguiente línea al fichero `/etc/resolv.conf`:

	search lpic.lan

En el equipo cliente *centos3* ejecutamos los siguientes comandos:

1. Actualizamos la información del fichero `/etc/hosts`:

	192.168.33.10	server.lpic.lan server
	192.168.33.11	nfserver.lpic.lan nfserver	
	127.0.0.1	client.lpic.lan cliente

2. Cambiamos el *hostname* de la máquina:

	# hostname cliente

Tal y como se comprueba en la sesión grabada, la instalación del servidor en *server* ha sufrido un fallo que nos obliga a realizar una desinstalación:

	# ipa-server-install --uninstall

Por lo que se vuelve a intentar de nuevo la instalación del servidor de *ipa*.

Mientras, en el cliente montamos la unidad NFS remota, e instalamos el paquete para gestionar las *acls* de NFS:

	# mount nfserver:/shared /mnt

	# yum install nfs4-acl-tools -y
	# man nfs4_acl
	# nfs4_getfacl /mnt
	[...]
	 
Mientras continúa la instalación del servidor *ipa*, aprovechamos para instalar los clientes en *nfserver* y *cliente*:

	# yum install ipa-client -y

El segundo intento de instalar el servidor de *ipa* también se ha parado antes de finalizar. Se prueba con la instalación previa de los siguientes paquetes:

	# yum install -y krb5-server pam_krb5 krb5-workstation

> Llegados a este punto de la sesión, y revisando las notas anteriores respecto a la memoria RAM del servidor y de su fichero `/etc/hosts` se ha completado con éxito la instalación de *FreeIPA Server*.

Desde el equipo cliente, y usando el navegador web nos conectaremos al panel de administración del servidor con las credenciales introducidas durante el proceso de instalación del mismo.

	https://server.lpic.lan
	admin
	<contraseña>

Desde el servidor de NFS ejecutamos los siguientes comandos:

	# kinit admin
	Password for admin@LPIC.LAN:
 	# klist -l
	# klist -k
	
Antes de continuar, debemos proceder con la instalación del cliente de *freeipa* (se había instalado el paquete pero falta la ejecución del mismo)

	# ipa-client-install
	[...]
	Provide the domain name of your IPA server (ex: example.com): lpic.lan
 	Provide your IPA server name: server.lpic.lan
	[...]
	Proceed with fixed values and no DNS discovery? [no]: yes
	[...]
	Continue to configure the system with these values? [n]: yes
	User authorized to enroll computers: admin
	Password for admin@LPIC.LAN:
	[...]
	The ipa-client-install command was successful

Ahora, desde el panel de administración web debe aparecer el equipo `nfserver.lpic.lan` en la lista de *Hosts*.

Repetimos el proceso de instalación del cliente de *freeipa* en el equipo cliente, del mismo modo que se hizo en `nfserver`.

Volvemos al panel de adminstración para añadir un nuevo servicio *nfs* asociado a nuestro equipo *nfserver.lpic.lan*, marcando la opción de *Force* en el cuadro de diálogo. Y ejecutamos el siguiente comando en `nfserver`:

	# ipa-getkeytab -s server.lpic.lan -p nfs/nfserver.lpic.lan -k /etc/krb5.keytab
	Keytab successfully retrieved and stored in: /etc/krb5.keytab
	# systemctl start nfs-secure

Después de realizar todos los pasos anteriores estaremos en disposición de realizar el montaje de la unidad compartida en NFS desde el equipo cliente haciendo uso de un *tickect* de Kerberos, proporcionado por *FreeIPA*:

	# systemctl start nfs-secure
	# kinit admin
	# klist
	# mount -o sec=krb5 nfserver:/shared /mnt

Tal y como se observa en la sesión, sólo se puede tener acceso al recurso compartido si estamos en posesión del *ticket*.

## Sesión 17 - FreeIPA y autenticación

En esta sesión se hace uso de las notas que están disponibles en [HackMD](https://hackmd.io/@gaioso/lpic3_notes), partiendo del mismo fichero `Vagrantfile`.

Aunque no está dentro del fichero `Vagrantfile`, también haremos uso de una tercera máquina virtual con un entorno gráfico, y así poder acceder al panel de administración web de *FreeIPA*.

Una vez levantadas las máquinas, y revisada la configuración del fichero `/etc/hosts`, añadimos la siguiente línea al fichero `/etc/resolv.conf` en el *servidor*:

	search lpic.lan

pero en en cliente haremos los siguientes cambios en dicho fichero:

	nameserver 192.168.1.10
	search lpic.lan

A diferencia de la sesión anterior, en esta práctica vamos a instalar el paquete de *ipa-server* que incorpora un servicio *dns*.

Ejecutamos la instalación del servidor de *freeipa*:

	# yum install ipa-server ipa-server-dns -y
	# ipa-server-install --setup-dns

> Aceptamos los valores por defecto e introducimos las contraseñas necesarias.

En el cliente ejecutamos los siguientes comandos:

> Debemos esperar a que finalice la instalación de *bind* en el servidor para que podamos ejecutar la instalación en el cliente.

	# yum groupinstall "Directory client"

### Añadir usuarios

En primer lugar obtenemos un ticket:

	# kinit admin
	
y procedemos con el proceso de creación de un nuevo usuario:

	# ipa user-add ldapuser
	First name: Ldap
	Last name: User
	[...]

y le asignamos una contraseña:

	# ipa passwd ldapuser
	New Password:

> **NOTA** La configuración manual no funcionará correctamente hasta que se instale el cliente de *freeipa*.
	
Desde el equipo cliente, y una vez instalado el grupo *Directory client*, vamos a configurar la autenticación:

	# systemctl stop nslcd.service
	# systemctl mask nslcd.service		
	# yum remove nss-pam-ldapd.x86_64	# Evita conflictos durante la instalación posterior

	# yum install pam_ldap pam_krb5 -y
	# authconfig --test		# Comprueba el estado de PAM
	# authconfig-tui

Marcamos las siguientes opciones:

* Use Shadow Passwords
* Use LDAP Authentication
* Use Kerberos
* Local authorization is sufficient

En la ventana de *LDAP Settings*:
	
	[*] Use TLS
	ipa.lpic.lan
	dc=lpic,dc=lan

En la ventana *Kerberos Settings*:

	Realm: LPIC.LAN
	KDC: ipa.lpic.lan
	Admin Server: ipa.lpic.lan
	[*] Use DNS to resolve hosts to realms
	[*] Use DNS to locate KDCs for realms


Copiamos el fichero del certificado de la *CA* desde el servidor al cliente:

	# scp ipa.lpic.lan:/etc/ipa/ca.crt /etc/openldap/cacerts/	# Desde el cliente

En este momento, se procede con la instalación del cliente de *freeipa* y se resuelven los problemas anteriores. Comprobamos la disponibilidad del usuario creado en el servidor:

	# getent passwd ldapuser
	[...]
	# su - ldapuser

Para que se pueda crear el directorio del usuario de forma automática debemos añadir el módulo PAM a la pila de *system-auth*:

	# vi /etc/pam.d/system-auth
	[...]
	session	  optional	pam_mkhomedir.so skel=/etc/skel  umask=077

También podemos iniciar una sesión *ssh* en el servidor con las credenciales del usuario *ldapuser*:

	# ssh ldapuser@ipa.lpic.lan

o bien, solicitar un ticket y conectar sin usar las credenciales anteriores:

	# kinit ldapuser
	# ssh ldapuser@ipa.lpic.lan

## Sesión 18 - IPAreplica

En este laboratorio hemos añadido una máquina virtual con una instalación de Windows Server 2016 en versión *trial*, y una tercera máquina con *centOS* que es donde instalaremos la réplica de nuestro servidor de *freeipa*.

Desde el servidor *ipa* añadimos a nuestro cliente como miembro del grupo *ipaservers*:

	# ipa help topics
	# ipa help host

	# ipa hostgroup-add-member ipaservers --hosts cli.lpic.lan
	# ipa hostgroup-find
	[...]
	# ipa hostgroup-show ipaservers
	[...]

Desde nuestra máquina cliente, realizamos la instalación del servidor de *freeipa* antes de que se pueda convertir en una réplica:

> Comprobar que los relojes de las máquinas se encuentran sincronizados y configurados correctamente.
> El *resolv* de los equipos cliente apunten al servidor *ipa.lpic.lan* (192.168.1.10)

	# yum install ipa-server -y
	# ipa-replica-install

Como aparece un error al ejecutar el comando de creación de la réplica hay que realizar algunos cambios en la zona DNS, que se pueden visualizar en el vídeo, ya que son realizadas a través de la interfaz web del panel de administración.

Como la instalación de la réplica no es posible en el equipo cliente (cli), lo haremos en la máquina *centos3* (replica)

Añadimos una nueva entrada en la zona DNS, desde el panel de administración.

Desde el servidor añadimos ese *host*:

	# ipa host-add replica.lpic.lan --random

Usaremos la *Random password* para el proceso de instalación de la réplica.

	# ipa hostgroup-add-member ipaservers --hosts replica.lpic.lan

Nos vamos al equipo *replica* para ejecutar las siguientes instalaciones:

	# yum install ipa-server -y
	# ipa-replica-install -p <password> --server ipa.lpic.lan --domain lpic.lan --realm LPIC.LAN
	
> Está pendiente la resolución del problema que aparece en el momento de instalar la réplica en los *host*.

### Configuración de Windows Server

En la última parte del vídeo se muestran los pasos necesarios para que se pueda iniciar sesión desde una máquina con Windows. Debido a que todas las instrucciones se realizan desde el entorno de Windows Server se recomienda su visualización directamente en el vídeo.

Los comandos a ejecutar desde PowerShell están detallados en las notas:

https://hackmd.io/MmqSgEVjTVeeogNzkDWWYg#Sesi%C3%B3n-en-Windows-con-usuario-FreeIPA


