# Herramientas IDS

## Fichero Vagrantfile

```
Vagrant.configure("2") do |config|

         config.vm.define "nodo01" do |vm|
           vm.vm.box = "ubuntu/bionic64"
           vm.vm.hostname = "nodo01"
           vm.vm.network "private_network", ip: "192.168.1.10", virtualbox__intnet: "intnet"
         end

         config.vm.define "nodo02" do |vm|
           vm.vm.box = "ubuntu/bionic64"
           vm.vm.hostname = "nodo02"
           vm.vm.network "private_network", ip: "192.168.1.20", virtualbox__intnet: "intnet"
         end
end
```

Es recomendable realizar una actualización de los repositorios nada más iniciar las máquinas virtuales:

	# apt update

## Control de permisos especiales en ficheros

Como es sabido, en nuestro sistema GNU/Linux existen varios ficheros que contienen permisos especiales, que por ejemplo, permiten su ejecución con los permisos de administrador del sistema:

	$ ls -l `which password`

Buscamos el paquete con el nombre `sxid`, y los instalamos:

	# apt search sxid
	# apt install sxid
	# man sxid

Para localizar los ficheros que pertenecen a un determinado paquete, ejecutamos el siguiente comando:

	# dpkg -L sxid

De esta forma, podemos localizar el fichero de configuración que incorpora esta herramienta; `/etc/sxid.conf`. En el fichero `/etc/default/sxid` se pueden establecer algunos valores de configuración global, que además serán usados a la hora de ejecutar este comando en el *cron* diario.

Ejecutamos el comando con las siguientes opciones:

	# sxid -c /etc/sxid.conf -k
	[...]
	No changes found

Este último mensaje confirma que no se han modificado los permisos a los ficheros monitorizados. En el fichero `/var/log/sxid.log` se pueden visualizar los ficheros que se han monitorizado, junto con un *hash*.

Si nos desplazamos al directorio raíz de nuestro sistema de ficheros y ejecutamos de nuevo el comando con la opción `-l` obtendremos un listado de los ficheros que han sido inspeccionados:

	# cd /
	# sxid -c /etc/sxid.conf -k -l

## Detectar conexiones no autorizadas

En este apartado veremos el funcionamiento de *PortSentry*, que nos va permitir un control sobre las conexiones establecidas en las intefaces de red. Comenzamos con la instalación del paquete `portsentry`:

	# apt install `portsentry`

Podemos comprobar las primeras actividades llevadas a cabo por la herramienta a través de las entradas en el *log* del sistema:

	# grep portsentry /var/log/syslog

En ese listado de puertos aparecen aquellos que mantienen alguna conexión activa en nuestra máquina. Podemos verificar si corresponden con alguno de los puertos que tenemos disponibles en local con el comando `nmap`:

	# nmap -sT localhost

A través del fichero de configuración `/etc/portsentry/portsentry.conf` iremos activando alguna de sus opciones, por ejemplo el bloqueo de los escaneos de los puertos tal y como se hizo en el comando anterior:

	# vi /etc/portsentry/portsentry.conf
	BLOCK_UDP="1"
	BLOCK_TCP="1"

y en el mismo fichero, activamos la creación de reglas en nuestro *firewall* con el comando `iptables`:

	KILL_ROUTE="/sbin/iptables -I INPUT -s $TARGET$ -j DROP"

por último, comprobamos que también se activará un bloqueo de la IP atacante a través de *TCP Wrappers*.

	KILL_HOSTS_DENY="ALL: $TARGET$ : DENY"

Para probar la nueva configuración, debemos reiniciar el servicio:

	# systemctl restart portsentry

Si ahora probamos a ejecutar un escaneo de puertos desde el *nodo02* ya no obtendremos ninguna respuesta:

	# nmap -sT 192.168.1.10

Además, podemos comprobar como se ha añadido una línea de bloqueo en el fichero `/etc/hosts.deny` en el *nodo01*:

	# cat /etc/hosts.deny
	[...]
	ALL: 192.168.1.20 : DENY

> A mayores del bloqueo anterior, también se crea un ruta estática en nuesto *nodo01* para que no se puedan realizar conexiones con el equipo atacante. Hay que tener cuidado con esta ruta por si queremos volver a desactivarla.

Para continuar, vamos a establecer nuestra red interna como red de confianza, añadiendo al siguiente línez en el fichero `/etc/portsentry/portsentry.ignore.static`:

	#0.0.0.0
	192.168.1.20

En este punto, también debemos comprobar el estado de nuestra tabla de enrutamiento, y eliminar las posibles rutas estáticas que se hayan añadido en pasos anteriores.

	# ip route del 192.168.1.20

A mayores de la información enviada al sistema de *log* del sistema, esta herramienta también almacena un historial de su actividad en el fichero `/var/lib/portsentry/portsentry.history`.

## Shorewall

Referencia: https://www.shorewall.org

Para seguir con el laboratorio, es necesario añadir una nueva interfaz a nuestro *nodo01* para que se pueda conectar con la máquina anfitrión, por lo que añadimos la siguiente línea a la sección de *nodo01* en el fichero `Vagrantfile`:

	vm.vm.network "private_network", ip: "192.168.4.10"

Después de reiniciar el *nodo01*, iniciamos un navegador web (Firefox, por ejemplo) en nuestra máquina anfitrión, la cual ya podrá conectar con la IP `192.168.4.10`.

En el *nodo01* ejecutamos los siguientes comandos para instalar *apache2* y *openssl*:

	# apt install apache2 -y
	# apt install openssl -y
	
	# mkdir /etc/apache2/ssl
	# openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/apache2/ssl/nodo01.key -out /etc/apache2/ssl/nodo01.crt
	[...]
	# a2enmod ssl
	# a2ensite default-ssl.conf

	# vi /etc/apache2/sites-enabled/default-ssl.conf

	ServerName 192.168.4.10:443
	SSLCertificateFile	/etc/apache2/ssl/nodo01.crt
	SSLCertificateKeyFile	/etc/apache2/ssl/nodo01.key

	# apache2ctl restart
	
Desde el navegador que hemos abierto más arriba, accedemos a la siguiente dirección: `https://192.168.4.10`, y aceptamos el certificado.
	
Instalamos el paquete de *shorewall*:

	# systemctl stop portsentry.service
	# apt install shorewall -y

Antes de poder ejecutar el programa recién instalado, debemos realizar algunos ajustes previos en su configuración:

	# cp /usr/share/doc/shorewall/examples/three-interfaces/interfaces /etc/shorewall/
	# cp /usr/share/doc/shorewall/examples/three-interfaces/zones /etc/shorewall/
	# cp /usr/share/doc/shorewall/examples/three-interfaces/policy /etc/shorewall/
	# cp /usr/share/doc/shorewall/examples/three-interfaces/rules /etc/shorewall/
	
Debemos indicar en algunos ficheros los valores que tengo en mi máquina virtual, por ejemplo en `interfaces` modificamos la columna *INTERFACE* con los siguientes valores:

	INTERFACE
	enp0s3
	enp0s8
	enp0s9

El fichero `zones` se deja intacto sin realizar ninguna modificación, y en el `policy` también se deja sin cambios, y se observa como se permite, o no, según la dirección del tráfico de la red. Por último, en el fichero `rules` estarán definidas aquellas reglas más específicas que afectan a un servicio, puerto, etc., de forma muy similar a como se hace con `iptables`.

Antes de iniciar el servicio, podemos realizar una comprobación de la configuración con el siguiente comando:

	# shorewall check

> Debemos actuar con precaución a la hora de iniciar el servicio, ya que al no realizar ningún cambio en la configuración por defecto de las `rules`, nuestro tráfico *ssh* será bloqueado y nos quedaremos sin conexión con la máquina virtual

> Otros recursos: [iptables-boilerplate](https://github.com/bmaeser/iptables-boilerplate)

Si solo modificamos la política que afecta a nuestra interfaz de red para *ssh*, no es suficiente para que siga activa nuestra conexión desde el exterior:

	# vi /etc/shorewall/policy

	net	all	ACCEPT	$LOG_LEVEL

Por lo que debemos añadir la siguiente regla al fichero `rules`:

	SSH(ACCEPT)	net	$FW

> Realizar las pruebas necesarias para ajustar la configuración a *ssh*

## OSSEC

Referencia: [ossec.net](https://www.ossec.net/)

## SNORT

Referencia: [snort.org](https://www.snort.org/)

Aunque esta herramienta está disponible bajo una licencia libre, existe un modelo de negocio que nos proporciona un conjunto de reglas actualizadas bajo una suscripción.

La instalamos con el siguiente comando:

	# apt install snort -y
	[...]
	Interface(s) which Snort should listen on:
	enps08
	Address range for the local network:
	192.168.1.0/24
	
Y la ejecutamos con los siguientes parámetros para poder escanear el tráfico de nuestro servidor web *apache2*:

	# snort -v -i enps09	# con la opción `-d` visualizamos el contenido de los paquetes

También podemos almacenar la información de *log* en un fichero determinado, por ejemplo:

	# mkdir /var/log/snort/logs_snort
	# snort -vd -i enps09 -l /var/log/snort/logs_snort/

Para poder visualizar correctamente esa información almacenada debemos ejecutar el comando `snort` con las siguientes opciones:

	# snort -d -v -r logs_snort/snort.log.<...>

## AIDE

Referencia: [aide.github.io](https://aide.github.io)

Esta herramienta gestiona la integridad de ficheros y directorios de nuestro sistema.

	# apt install aide -y
	# vi /etc/default/aide
	CRON_DAILY_RUN=yes

En el siguiente fichero de configuración añadimos dos líneas al final del mismo para excluír dos directorios del escaneo:
	
	# vi /etc/aide/aide.conf
	
	!/var/lib/lxcfs
	!/var/log/journal
	 
Creamos la base de datos inicial (será guardada junto con el resto de versiones en el directorio `/var/lib/aide`):
	
	# aideinit
	[...]

Sobreescribimos esta nueva base de datos sobre la versión anterior:

	# cp -p /var/lib/aide/aide.db.new /var/lib/aide/aide.db
	# aide.wrapper --check
	# touch test.file
	# aide.wrapper --check

## rkhunter

	# apt install rkhunter
	# rkhunter --propupd
	# rkhunter --check -sk
	[...]


