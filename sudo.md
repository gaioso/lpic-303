
	man sudo

	man sudoers

Por ejemplo, para actualizar el sistema debemos hacer uso de privilegios de administrador a través del comando `sudo`:

	$ sudo yum update

## visudo

Como estamos trabajando en una máquina CentOS, debemos comprobar si nuestro usuario actual pertenece al grupo *wheel*, ya que sus miembros son los que tienen permiso para ejecutar comandos con permiso de administrador.
En la sesión grabada, el usuario *vagrant* no pertenece al grupo *wheel*. Algo que indicaría que no tiene los permisos necesarios para la ejecución de comandos como administrador. Sin embargo, cuando observamos el contenido del directorio `/etc/sudoers.d/`, vemos que existe un fichero `vagrant`con el siguiente contenido:

	%vagrant ALL=(ALL) NOPASSWD: ALL

Donde el primer *ALL* hace referencia a los hosts en los cuales se aplica esta configuración. El segundo *(ALL)* hace referencia a los usuarios a los cuales se pueden cambiar los usuarios del grupo *vagrant* para ejecutar los comandos, y por último, el *ALL* del final indicar que se puede ejecutar cualquier comando. Y, por supuesto, todo ello sin tener que introducir nuestra contraseña de usuario (*NOPASSWD:*).

Todos los usos que se hagan con el comando `sudo` se quedan registrados en el fichero `/var/log/secure`, que es algo que me ayudará a realizar auditorías de su uso por parte de los miembros del grupo *vagrant*.

Para realizar las siguientes prácticas, vamos a crear algunos usuarios de prueba:

	$ sudo adduser tuxblanco
	$ sudo passwd tuxblanco
	$ sudo usermod -aG wheel tuxblanco

En el fichero de configuración `sudoers` podemos hacer uso del concepto de *Alias* para referirnos a un grupo de objetos, como pueden ser los usuarios en este ejemplo:

	User_Alias ADMINS = tuxblanco, tuxverde
	ADMINS ALL=(ALL) ALL

Por lo tanto, el usuario `tuxverde` también podrá ejecutar comandos de administrador.

	$ sudo adduser tuxverde && sudo passwd tuxverde
	$ su - tuxverde

Ahora, creamos un *Alias* para otro conjunto de usuarios:

	User_Alias SWADMINS = tuxazul
	Cmnd_Alias SOFTWARE = /bin/rpm, /usr/bin/up2date, /usr/bin/yum
	SWADMINS ALL=(ALL) SOFTWARE


	$ sudo adduser tuxazul && sudo passwd tuxazul
	$ su - tuxazul
	$ sudo yum update		# Funcionará
	$ sudo systemctl stop httpd	# No funcionará

> Vídeo 07 - sudo y remote logging

Comenzamos este vídeo con la creación de un nuevo alias de usuarios para que puedan usar los comandos agrupados en la gestión de los servicios de nuestra máquina.

	User_Alias SERVICEADMINS = tuxnegro
	Cmnd_Alias SERVICES = [...]
	SERVICEADMINS ALL=(root) SERVICES

	$ sudo adduser tuxnegro && sudo passwd tuxnegro
	$ su - tuxnegro

Si queremos comprobar la configuración que tenemos activada en el fichero `sudoers`, podemos ejecutar el siguiente comando:

	$ sudo -l
	[...]

Tal y como se ha configurado *sudo* para el usuario *tuxnegro* tengo permitido ejecutar el siguiente comando:

	$ sudo systemctl status

pero no el siguiente:

	$ sudo systemctl status httpd

Para modificar esta situación, debemos añadir un asterisco (\*) en la configuración de *sudo*, por ejemplo:

	... systemctl status * ...

El siguiente cambio a realizar es el siguiente:

	SERVICEADMINS centos1=(root) SERVICES
	SERVICEADMINS centosr3=(ALL) SERVICES

Por lo que en la máquina *centos1* se pueden ejecutar los comandos SERVICES con el perfil de *root*, y en *centos3* se pueden ejecutar con el perfil de cualquier usuario.

Para probar esta configuración debemos realizar una copia del fichero `sudoers` en *centos2*.

Durante los siguientes ejercicios se hace necesario que el usuario *tuxnegro* pueda instalar nuevo software en *centos3*, por lo que modificamos el fichero `sudoers` en dicha máquina, añadiendo el alias SOTFWARE a la lista permitida para ese usuario:

	SERVICEADMINS centos3=(ALL) SERVICES,SOFTWARE

Probando su nueva configuración con la instalación de *apache*:

	$ su -u tuxnegro
	$ sudo yum install httpd -y

Ahora, creamos un alias para varios *host*:

	Host_Alias	SERVERS = centos1, centos2, centos3
	User_Alias	UDISKS = tuxverde
	Cmnd_Alias	STORAGE = [...]

	UDISKS	SERVERS=(root)	STORAGE

De esta forma, los usuarios del *alias* UDISKS (en nuestro ejemplo *tuxverde*), podrán ejecutar los comandos del *alias* STORAGE con el perfil de *root* en las máquinas del *alias* SERVERS.

Cuando ejecutamos el comando `sudo`, en realidad es como si iniciaramos una nueva sesión con el perfil del usuario indicado. Y como tal sesión, se cargan algunas variables de entorno, entre las cuales podemos destacar la que determina el tiempo de vida de la contraseña introducida para cambiar el perfil de usuario.

	Defaults timestamp_timeout = 0		# Se pìde identificación en cada ejecución del comando `sudo`

También se puede restringir el valor anterior a un único usuario, por ejemplo *tuxverde*:

	Defaults:tuxverde timestamp_timeout = 0

o incluso a un *alias* de usuarios:

	Defaults:UDISKS timestamp_timeout = 0
	

