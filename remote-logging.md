# Remote Logging

Un escenario muy frecuente es aquel en el que una mala configuración de una aplicación provoca que el directorio `/var/log` ocupe todo el espacio disponible, provocando un fallo en el servicio de gestión de *logs*. El *logging remoto* no tiene más secreto que el guardado de esta información en un equipo remoto, en lugar de hacerlo en el sistema de ficheros local.

En este laboratorio vamos a enviar la información de *log* hacia un servidor remoto, que centralizará la información recibida desde las máquinas de nuestra red. Dicho envío se hará con, y sin, capa de seguridad añadida al envío.

Necesitamos dos máquinas, y usaremos las dos CentOS que se venían utilizando en los laboratorios anteriores.

Lo primero que vamos a configurar será el fichero `/etc/hosts`, en el cual añadimos las siguientes líneas:

	192.168.33.10 centos1 centos1.lpic.lan
	192.168.33.12 centos3 centos3.lpic.lan

Este cambio lo haremos tanto en *centos1* como en *centos3*. En este escenario, *centos1* actuará como cliente del servicio, y *centos3* será el equipo que reciba la información.

Comenzamos con los cambios en la configuración del servicio `rsyslog` en el cliente:

	$ sudo vi /etc/rsyslog.conf
	> Añadimos la siguiente línea
	*.* @192.168.33.12
	
	$ sudo systemctl restart rsyslog.service
	
Y, en el servidor también debemos ajustar la configuración de `rsyslog`:

	$ sudo vi /etc/rsyslog.conf
	> Quitamos los comentarios a las siguientes líneas
	$ModLoad imudp
	$UDPServerRun 514
	
	$ sudo systemctl restart rsyslog.service

Para realizar una prueba del envío hacia el servidor, ejecutamos el siguiente comando en el cliente:

	$ logger -p syslog.info "Probando la conexion..."
	
Para que el envío desde el cliente se haga a través de una conexión TCP, debemos añadir un carácter *@* en la línea del fichero `rsyslog.conf`.

	*.* @@192.168.33.12
	
Y, en el servidor quitamos el comentario a las siguientes líneas:

	$ModLoad imtcp
	$InputTCPServerRun 514

## Laboratorio con journald

Para realizar esta práctica debemos crear un nuevo laboratorio con máquinas *Debian*. Usaremos el siguiente fichero *Vagrantfile*:

```
Vagrant.configure("2") do |config|

         config.vm.define "debian1" do |debian1|
           debian1.vm.box = "debian/stretch64"
           debian1.vm.network "private_network", ip: "192.168.33.10", virtualbox__intnet: "interna"
           debian1.vm.hostname = "debian1"
         end

         config.vm.define "debian2" do |debian2|
           debian2.vm.box = "debian/stretch64"
           debian2.vm.network "private_network", ip: "192.168.33.11", virtualbox__intnet: "interna"
           debian2.vm.hostname = "debian2"
         end
end
```

En *debian1*, ejecutamos los siguientes comandos:

	$ sudo apt install systemd-journal-remote
	$ sudo vi /etc/systemd/journal-upload.conf
	[Upload]
	URL=http://192.168.33.11
	
	$ sudo systemctl restart systemd-journal-remote
	
Y, en *debian2*, debemos modificar la *unit* que gestiona el *journal-remote*.

	$ sudo apt install systemd-journal-remote
	$ sudo systemctl edit systemd-journal-remote
	[Service]
	ExecStart=
	ExecStart=/lib/systemd/system-journal-remote --listen-http=-3 --output=/var/log/journal/remote

> Cuando asignamos un valor nulo a `ExecStart` es para evitar un error en la asignación posterior.
	
	$ sudo mkdir -p /var/log/journal/remote
	$ sudo chown systemd-journal-remote /var/log/journal/remote
	$ sudo systemctl restart systemd-journal-remote
	
	$ sudo journalctl -D /var/log/journal/remote -f
	
Dejamos a la escucha el servicio en *debian2*, y volvemos a *debian1* para hacer la prueba de envío:

	$ logger -p syslog.info "Envio de log desde debian1"

Para poder asegurar la información que se intercambia entre las máquinas del laboratorio. Es el mismo concepto que se ha visto en el tema de SSH, haciendo posible que un puerto local se conecte con un puerto remoto a través de una conexión SSH.

En *debian1*:

	$ ssh -f -L 9966:127.0.0.1:19532 192.168.33.11 sleep 300

Una vez creado el túnel, debemos modificar la configuración de *journal-upload*, añadiendo el puerto en la línea de la URL:

	URL=http://127.0.0.1:9966
	
	$ sudo systemctl restart systemd-journal-remote

> NOTA: Al final de este vídeo no funciona correctamente la conexión a través del túnel SSH. La solución a dicha configuración se encuentra en el siguiente vídeo, y esencialmente es debido a un error a la hora de gestionar el servicio responsable de los envíos desde el cliente (se debe usar `systemd-journal-upload`).

Por lo tanto, en el cliente *debian1* debemos configurar el servicio `systemd-journal-upload`, y en el servidor *debian2* el servicio `systemd-journal-remote`.

	# systemctl start systemd-journal-remote	# debian2
	# systemctl start systemd-journal-upload	# debian1

En el cliente *debian1* debemos modificar el siguiente fichero:

	# vi /etc/systemd/journal-upload.conf

	URL=http://192.168.33.11

	# systemctl start systemd-journal-upload
	# logger -p syslog.info "Debian1 sending logs...."


**Por el momento dejamos en pausa el laboratorio con las máquinas Debian, y regresamos al entorno CentOS.**

## RSYSLOG

Recordamos que en el equipo cliente estaba configurado el envío hacia el servidor de todos los mensajes de *log*, con la siguiente línea en el fichero `etc/rsyslog.conf`

	*.* @@192.168.33.12

Y en el servidor debemos activar las líneas que corresponden a las secciones *Provides UDP/TCP syslog reception*, en el fichero `/etc/rsyslog.conf`.

Debemos proteger la información que se está enviando en claro, que es lo que se realizará en la siguiente práctica.

En el equipo cliente, instalamos la documentación de *rsyslog* y un navegador para poder visualizarla:

	$ sudo yum install rsyslog-doc elinks
	$ cd /usr/share/doc/rsyslog-8.24.0/html/
	$ elinks index.html

Cuando tengamos que establecer conexiones seguras entre diferentes máquinas es fundamental que tengan los relojes bien configurados y sincronizados, para evitar errores de diferencia temporal entre ellos.

	# systemctl status chronyd

Comenzamos instalando el driver *gtls* en el equipo cliente, y los certificados

	# yum install gnutls-utils
	# certtool --generate-privkey --outfile ca-key.pem
	# chmod 400 ca-key.pem
	# certtool --generate-self-signed --load-privkey ca-key.pem --outfile ca.pem
	[...]

Las opciones que se deben usar en la generación de este certificado están explicadas en el vídeo.

	# certtool --generate-privkey --outfile centos3-key.pem
	# certtool --generate-request --load-privkey centos3-key.pem --outfile centos3-request.pem
	[...]
	# certtool --generate-certificate --load-request centos3-request.pem --outfile centos3-cert.pem --load-ca-certificate ca.pem --load-ca-privkey ca-key.pem
	[...]

Ahora en el servidor, ejecutamos los siguientes comandos:

	# yum install gnutls-utils
	# mkdir /etc/rsyslog-keys
	
Copiamos los ficheros recién creados desde el cliente hacia el servidor:

	# scp centos3*.pem centos3:/etc/rsyslog-keys

	# yum install rsyslog-gnutls
	# mkdir /etc/rsyslog-keys
	# cp ca.pem /etc/rsyslog-keys
	# vi /etc/rsyslog.d/log-client.conf
	
	$DefaultNetStreamDriverCAFile /etc/rsyslog-keys/ca.pem
	$DefaultNetStreamDriver gtls
	$ActionSendStreamDriverMode 1
	$ActionSendStreamDriverAuthMode anon
	*.*     @@(o)centos3.lpic.lan:6514

Y en el servidor:

	# vi /etc/rsyslog.d/log-server.conf
	
	$DefaultNetStreamDriver gtls
	$DefaultNetstreamDriverCAFile /etc/rsyslog-keys/ca.pem
	$DefaultNetstreamDriverCertFile /etc/rsyslog-keys/centos3-cert.pem
	$DefaultNetstreamDriverKeyFile /etc/rsyslog-keys/centos3-key.pem

	$ModLoad imtcp

	$InputTCPServerStreamDriverMode 1
	$InputTCPServerStreamDriverAuthMode anon
	$InputTCPServerRun 6514

Enviamos desde el cliente el fichero de la CA creado anteriormente:

	# scp ca.pem centos3:/etc/rsyslog-keys/

En este punto finaliza la configuración y procedemos a reniciar el servicio:

	# systemct restart rsyslog.service

## Establecer filtros en mensajes de log

> Referencia: https://www.rsyslog.com/doc/v8-stable/configuration/filters.html

En el servidor creamos un fichero de configuración:

	# vi /etc/rsyslog.d/lpiclan.conf

	:fromhost, isequal, "centos1" /var/log/centos1/messages
	& ~

	# systemctl restart rsyslog

## Rotación de ficheros de log

Para poder gestionar los ficheros de log, de forma periódica se ejecutan los *logrotate* sobre los ficheros de log para poder realizar diferentes operaciones sobre los mismos. Normalmente, se comprimen y se guarda un número limitado de copias pasadas.

Podemos observar parte de la configuración existente en *cron* en el fichero `/etc/cron.daily/logrotate`.

El fichero de configuracion principal es `/etc/logrotate.conf`. Y luego existen configuraciones propias para cada fichero en el directorio `/etc/logrotate.d`.

En el siguiente ejercicio, se aplicará un *logrotate* sobre un fichero de texto que se gestionará de forma manual (o sea, que no estará controlado por *rsyslog*). En primer lugar, creamos el fichero de configuración para nuestra práctica:

	# vi /etc/logrotate.d/lpic.conf

	/var/lpic/test {
	  daily
	  rotate 2
	  size 512
	  compress
	}

	# > /var/lpic/test
	# logrotate /etc/logrotate.d/lpic --debug
	[...]

En este punto, si enviamos suficiente contenido al fichero `test` como para que su tamaño supere los 512 bytes, en la siguiente ejecución del comando `logrotate` veremos que se ejecutan los comandos necesarios para guardar las copias de respaldo.

## Output Channels

Con esta opción evitamos que sólo se ejecuten los comandos `logrotate` con la periodicidad establecida, y que se consiga monitorizar algunos parámetros de los ficheros de *log* en tiempo real. De este modo, se pueden detectar situaciones de riesgo en los periodos que hay entre las ejecuciones de *logrotate*.

Vamos a crear un *outchannel* para el ejemplo anterior:

	# vi /etc/logrotate.d/test.conf

	$outchannel	prueba, /var/lpic/test, 1024, /usr/sbin/logrotate /etc/rsyslog.d/lpic
	mail.*	$prueba

## Sesión 09 - rsyslogd, ELK y Fluentd

### Filtrado de mensajes

Para realizar la siguiente práctica vamos a configurar el servicio *rsyslogd* sin ninguna característica adicional. Usaremos la versión básica que se ha visto en capítulos anteriores.

Se configura el laboratorio para que *centos3* reciba los mensajes de *log* de la red. Y desde *centos1* y *centos2* se configura el envío de dichos mensajes hacia *centos3*. En este escenario, no se ha activado la capa TLS.

En primer lugar, creamos un fichero para configurar los filtros en el servidor *centos3*:

	# vi /etc/rsyslog.d/remotefilter.conf

	:fromhost, isequal, "centos1" /var/log/centos1/messages
	$~
	:msg, contains, "linus" /var/log/linus.log
	:hostname, isequal, "centos2" /var/log/centos2/messages

	# systemctl restart rsyslog.service

> En la línea 2 del fichero anterior hay un error tipográfico. Se de introducir `& ~` en lugar de `$~`.

Enviamos los mensajes de *log* desde nuestros equipos de la red.

	# logger -p local7.info "Configurando el servidor remoto de logging"		# centos1
	# logger -p local7.info "Envio desde centos2 con texto linus"			# centos2

> Parece que el servidor no reconoce *centos1* como un valor aplicable al selector `fromhost`, por lo que se hace uso de `fromhost-ip`. Para que funcione debemos actualizar el contenido del fichero `etc/hosts` añadiendo la entrada de *centos1*.

## Outuput Channels (continuación)

https://www.rsyslog.com/doc/v8-stable/configuration/output_channels.html

> **NOTA** - Aunque en el vídeo se han cometido diferentes errores de escritura, en las siguientes líneas ya aparecen corregidos.

En este ejercicio, utilizaremos una única máquina. En la cual vamos a configurar los canales a través del siguiente fichero:

	# vi /etc/rsyslog.d/channel.conf
	
	$outchannel canal,/var/log/linus.log,550,/sbin/logrotate /etc/logrotate.d/linus
	*.*:omfile:$canal

	# vi /etc/logrotate.d/linus
	
	/var/log/linus.log {
	  size 512
	  rotate 2
	  compress
	}

Y ahora enviamos contenido hacia el fichero de *log* con el siguiente comando:

	# echo "xxxxxxxxx.....xxxxx" >> linus.log
	# !!
	# !!

> **NOTA** El problema de enviar contenido al fichero `linus.log` con el comando `echo` es que *rsyslog* no tiene control sobre el mismo y no funciona el *outchannel*. Hay que enviar el contenido a través del comando `logger` tal y como se hace en el vídeo (hacia la mitad).

## Fluentd

Instalación en distribuciones RedHat: https://docs.fluentd.org/installation/install-by-rpm

	$ curl -L https://toolbelt.treasuredata.com/sh/install-redhat-td-agent3.sh | sh

Una vez instalado el agente, lo inciamos con el siguiente comando:

	$ sudo systemctl start td-agent.service

Siguiendo las instrucciones que aparecen en la documentación, ejecutamos la prueba de envío de *log* a través de HTTP.

	$ curl -X POST -d 'json={"json":"message"}' http://localhost:8888/debug.test

El mensaje enviado en el comando anterior, queda almacenado en el fichero `/var/log/td-agent/td-agent.log`.

Ahora vamos a seguir la documentación de *fluentd* para enviar los mensajes de *log* de *apache* a una base de datos *mongoDB*.

https://docs.fluentd.org/how-to-guides/apache-to-mongodb

1. Instalación de mongoDB

	# vi /etc/yum.repos.d/mongodb-org.repo

	[mongodb-org-3.4]
	name=MongoDB Repository
	baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/3.4/x86_64/
	gpgcheck=1
	enabled=1
	gpgkey=https://www.mongodb.org/static/pgp/server-3.4.asc

	# yum repolist
	# yum install mongodb-org -y

2. Instalación de apache y gcc

	# yum install httpd gcc -y

3. Instalación del plugin

	# /opt/td-agent/embedded/bin/fluent-gem install fluent-plugin-mongo
	
4. Fichero de configuración en *fluentd*

	# vi /etc/td-agent/td-agent.conf
	
Añadimos el siguiente contenido

	<source>
	  @type tail
	  path /var/log/httpd/access_log
	  pos_file /var/log/td-agent/apache2.access_log.pos
	  <parse>
	    @type apache2
	  </parse>
	  tag mongo.apache.access
	</source>

	<match mongo.**>
	  # plugin type
	  @type mongo
	
	  # mongodb db + collection
	  database apache
	  collection access
	
	  # mongodb host + port
	  host localhost
	  port 27017
	
	  # interval
	  <buffer>
	    flush_interval 10s
	  </buffer>
	
	  # make sure to include the time key
	  <inject>
	    time_key time
	  </inject>
	</match>

5. Reiniciamos el agente e iniciamos el servidor web *apache*, y la base de datos:

	# systemctl restart td-agent
	# systemctl start httpd mongod

> Para que pueda leer los ficheros de *log* de *apache* debemos modificar los permisos del siguiente directorio:

	# chmod a+x /var/log/httpd

6. Ejecutamos una prueba
	
	$ ab -n 100 -c 10 http://localhost/

7. Y por último comprobamos si se ha enviado la información de *log* a la base de datos.

	# mongo
	> use apache
	> db["access"].findOne();
	[...]

## ELK - Elasticsearch, Logstash y Kibana

https://www.elastic.co/es/what-is/elk-stack

Para realizar este laboratorio, en primer lugar debemos instalar *Java*.

	# yum install java-1.8.0-openjdk -y
	# cat <<HERE | tee /etc/yum.repos.d/elasticsearch.repo
    	[elasticsearch-6.x]
    	name=Elasticsearch repository for 6.x packages
    	baseurl=https://artifacts.elastic.co/packages/6.x/yum
    	gpgcheck=1
    	gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
    	enabled=1
    	autorefresh=1
    	type=rpm-md
    	HERE

	# yum repolist
	# yum install elasticsearch kibana logstash -y

Mientras se realiza la instalación en el servidor, podemos ajustar uno de nuestros clientes para que envíe su información de *log* al nuevo servicio. Para ello, modificamos el fichero `/etc/rsyslog.conf` con la siguiente línea:

	*.* @@192.168.33.12:5000

	# systemctl restart rsyslog

De vuelta en el servidor actualizamos *systemd*:

	# systemctl daemon-reload

Dependiendo de la cantidad de memoria asignada a nuestra máquina virtual quizás tengamos que incrementarla para poder levantar los siguientes servicios. En la práctica se han asignado 2GB a la máquina *centos3*:

	# systemctl enable --now elasticsearch kibana

Configuramos *logstash* antes de iniciarlo:

	# vi /etc/logstash/conf.d/logstash-syslog.conf

	input {
	  tcp {
	    port => 5000
	    type => syslog
	  }
	  udp {
	    port => 5000
	    type => syslog
	  }
	}
	
	filter {
	  if [type] == "syslog" {
	    grok {
	      match => { "message" => "%{SYSLOGTIMESTAMP:syslog_timestamp} %{SYSLOGHOST:syslog_hostname} %{DATA:syslog_program}(?:\[%{POSINT:syslog_pid}\])?: %{GREEDYDATA:syslog_message}" }
	      add_field => [ "received_at", "%{@timestamp}" ]
	      add_field => [ "received_from", "%{host}" ]
	    }
	    date {
	      match => [ "syslog_timestamp", "MMM  d HH:mm:ss", "MMM dd HH:mm:ss" ]
	    }
	  }
	}
	
	output {
	  elasticsearch { hosts => ["localhost:9200"] }
	  stdout { codec => rubydebug }
	}

> Referencia: https://www.elastic.co/guide/en/logstash/6.4/config-examples.html#_processing_syslog_messages

	# systemctl enable --now logstash

Para poder consultar el panel de control desde el exterior, se añadirá unha interfaz que tenga acceso desde el exterior de la máquina virtual. Aunque para que nos funcione este acceso debemos permitir las conexiones a Kibana.

	# vi /etc/kibana/kibana.yml

	server.host: "192.168.30.12"		# La interfaz que tiene conexión con el exterior

	# systemctl restart kibana

Y desde el exterior accedemos a la siguiente URL: http://192.168.30.12:5601
