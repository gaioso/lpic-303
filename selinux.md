# SELinux - 333.2 Mandatory Access Control 

Referencias:
* [SELinux Coloring Book](https://people.redhat.com/duffy/selinux/selinux-coloring-book_A4-Stapled.pdf)
* [Conceptos de SELinux](https://gitlab.com/gaioso/selinux/-/blob/master/Capitulo1.md)

Aunque haremos uso de CentOS en nuestros laboratorios, también se verá su uso en otras distribuciones diferentes y que no tiene relación directa con Red Hat, como puede ser Debian GNU/Linux.

Para nuestro laboratorio, sólo será necesario el uso de una única máquina virtual CentOS, tal y como hemos venido utilizando hasta el momento.

## Estado inicial de SELinux

Para poder comprobar el estado en el que se encuentra SELinux por defecto, debemos leer el contenido del siguiente fichero:

	# vi /etc/selinux/config
	[...]
	SELINUX=enforcing
	[...]

Tal y como se visualiza en el propio fichero, los tres posibles valores son:

1. enforcing
2. permissive
3. disabled

Con el comando `getenforce` se puede visualizar el estado activo en cada momento. Y con `setenforce` es posible modificar dicho estado. Y, con el comando `sestatus` obtendremos información ampliada respecto al estado de SELinux.

Para poder visualizar la información relativa al contexto del sistema de ficheros con el comando `ls` debemos hacer uso de la opción `-Z`:

	# ls -lZ

Y con el comando `id -Z` veremos la información relativa al usuario actual. De forma similar a las anteriores, para visualizar las etiquetas de los procesos ejecutamos el comando `ps -Zaux`.

## Práctica con servidor web

Para comenzar con la primera práctica, instalamos un servidor web:

	# yum group install -y "Basic web server"
	# systemctl start httpd
	# ps Zaux | grep http
	# ls -Z /var/www/

Tal y como se aprecia en la salida del último comando, se observa la diferencia entre el contexto que tienen los dos directorios, entendiendo que su propósito es diferente, ya que en el directorio `cgi-bin` se almacenarán ficheros para poder ser ejecutados con el servidor web (scripts).

Vamos a modificar la ubicación del *DocumentRoot* en nuestro servidor web, para ver las implicaciones que tendrán en el contexto de SELinux.

	# mkdir /web
	# ls -Zd /web
	# vi /etc/httpd/conf/httpd.conf
	[...]
	DocumentRoot "/web"
	[...]
	<Directory "/web">
	[...]
	<Directory "/web">
	[...]

	# vi /web/index.html
	<html>
	<body>
	<h1>Sesión de trabajo con SELinux</h1>
	</body>
	</html>

	# systemctl restart httpd

Para poder consultar el fichero *html* recién creado, procedemos a la instalación de un navegador web en modo texto:

	# yum install -y elinks
	# elinks http://localhost

En el fichero `/var/log/audit/audit.log` se podrá observar la actividad relacionada con SELinux. En esta práctica se puede observar que se está denegando el acceso al fichero `/web/index.html` por parte del servidor web.

	# grep AVC /var/log/audit/audit.log

Para poder mejorar la visualización del fichero `audit.log`, usaremos el comando `aureport`.

	# aureport -a

Antes de ajustar los parámetros del contexto de nuestro nuevo directorio `/web` podemos desactivar temporalmente la protección de SELinux, aunque se sigan registrando los mensajes:

	# setenforce permissive
	# elinks http://localhost

La solución ideal pasa por cambiar el contexto del directorio `/web`, en concreto, debe poseer el valor `httpd_sys_content_t`.

	# yum install policycoreutils-python -y
	# semanage fcontext -a -t httpd_sys_content_t "/web(/.*)?"
	# restorecon -Rv /web
	# ls -ldZ /web
	# setenforce 1
	# elinks http://localhost
	
Por lo tanto, ahora funciona correctamente el acceso al contenido del directorio `/web` a pesar de tener activado SELinux en modo *enforce*.

Para continuar con el manejo de utilidades para SELinux, procedemos a la instalación del siguiente paquete:

	# yum install setools -y
	# seinfo -t
	# man seinfo

## Práctica son ssh

En el siguiente ejercicio se va a modificar el puerto de escucha del servicio *ssh*, y será necesario ajustar los parámetros de SELinux que autoricen dicho cambio.

	# vi /etc/ssh/sshd_config
	Port 2020
	# systemctl restart sshd

Este cambio no es posible debido a las restricciones impuestas por SELinux, que podemos observar en el fichero de *log*:

	# aureport -a

Par continuar con el ejercicio, instalamos el siguiente paquete que nos proporciona nuevas herramientas:

	# yum install setroubleshoot -y
	# sealert -a /var/log/audit/audit.log

El último comando analiza los mensajes del fichero de *log*, y es capaz de ampliar información acerca de dicha alerta. Incluso proporciona los comandos a ejecutar para solucionar dicha incidencia. Tal y como se indica en el vídeo, el uso de dicho comando nos proporciona el motivo por el cual no es posible cambiar el puerto de escucha del servicio *ssh*, y como solucionarlo.

	# semanage port -a -t ssh_port_t -p tcp 2022
	# semanage port -l
	[...]
	# semanage port -l | grep ssh
	ssh_port_t		tcp	2022,22
	# systemctl restart sshd

## Práctica con ftp

Comenzamos con la instalación del servidor *ftp*:

	# yum install vsftpd -y
	# systemctl start vsftpd
	# yum install lftp
	# lftp localhost
	> user linus
	Password
	> ls -a
	> put /etc/hosts
	# vi /etc/vsfptd/vsftpd.conf
	anon_upload_enable=YES
	anon_mkdir_write_enable=YES
	# systemctl restart vsftpd

	# lftp localhost
	> cd pub
	/pub> put /etc/hosts
	Access failed

A pesar del cambio aplicado en el fichero de configuración del servicio `vsftpd`, no ha sido posible la subida del fichero por un usuario anónimo.

	# aureport -a
	# sealert -a /var/log/audit/audit.log

	# getsebool -a | grep ftp
	[...]
	# setsebool -P ftpd_anon_write on

	# mkdir /var/ftp/upload
	# chown ftp /var/ftp/upload
	# chmod 300 /var/ftp/upload 

	# lftp localhost
	> cd upload
	/upload> put /etc/hosts

Como sigue sin funcionar, debemos comprobar el contexto del directorio recién creado, ya que está siendo bloqueado desde SELinux.

	# sealert -a /var/log/audit/audit.log

Y ejecutamos los comandos necesarios para que se pueda hacer uso del nuevo directorio `upload` por parte del servicio `vsftpd`:

	# semanage fcontext -a -t public_content_rw_t /var/ftp/upload
	# restorecon -v /var/ftp/upload

En este momento, será posible subir el fichero `/etc/hosts` al directorio `upload` como usuario anónimo.

## SELinux en Debian

Instalamos los siguientes paquetes en la nueva máquina recién creada con Debian *stretch*:

	# apt install selinux-basics auditd
	# selinux-activate

Comprobamos el cambio aplicado en el fichero de configuración global de *grub*:

	# vi /etc/default/grub
	GRUB_CMDLINE ... security=selinux"

	# sestatus
	# ls -ldZ

## apparmor en Ubuntu

Hacemos uso de una máquina con Ubuntu *bionic*.

	$ cd /etc/apparmor.d
	$ vi usr.bin.man
	$ man 7 capabilities

	# aa-status

