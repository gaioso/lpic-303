# Sesión de presentación y configuración de red

Durante esta primera sesión se iniciarán las tareas de configuración del laboratorio a utlizar en las siguientes sesiones, y se realizará un repaso a los conceptos básicos de gestión de la red local en nuestras máquinas virtuales.

## Configuración de laboratorio con Vagrant

Al igual que se viene haciendo en el resto de cursos, vamos a crear un laboratorio gestionado con Vagrant.

	$ mkdir iptables
	$ cd iptables
	$ vi Vagrantfile

> Contenido del fichero *Vagrantfile*

```
Vagrant.configure("2") do |config|

         config.vm.define "centos1" do |centos1|
           centos1.vm.box = "centos/7"
           centos1.vm.network "private_network", ip: "192.168.33.10"
           centos1.vm.network "private_network", ip: "192.168.44.10", auto_config: false
           centos1.vm.hostname = "centos1"
         end

         config.vm.define "centos2" do |centos2|
           centos2.vm.box = "centos/7"
           centos2.vm.network "private_network", ip: "192.168.33.11"
           centos2.vm.network "private_network", ip: "192.168.44.11", auto_config: false
           centos2.vm.hostname = "centos2"
         end
end
```

	$ vagrant up
	
En este momento, tendremos creadas y levantadas las dos máquinas virtuales de nuestro laboratorio (centos1 y centos2)

	$ vagrant ssh centos1

## Configuración básica de red
	
Una vez que estamos conectados a nuestra máquina *centos1*, podemos comenzar a ejecutar los comandos necesarios para comprobar la configuración de red.

	$ ip address show
	
La interfaz *eth0* corresponde a la que estará conectada con el NAT de Virtualbox, y que nos proporciona salida hacia el exterior (Internet). Tal y como se indicaba en el fichero *Vagrantfile*, la interfaz *eth2* no tiene ninguna dirección IP configurada.

	$ ip address show dev eth2

Enviamos un *ping* a *centos2* para comprobar la conectividad:

	$ ping 129.168.33.11

Con el siguiente comando, seremos capaces de visualizar la IP (v4) asignada a una de las interfaces de nuestra máquina:
	
	$ ip address show dev eth1 | grep "inet " | awk '{ print $2 }'

## Tabla de enrutamiento

Ahora, vamos a comprobar el estado de nuestra tabla de enrutamiento:

	$ ip route

A mayores de la ruta por defecto (puerta de enlace), tendremos dos rutas estáticas para cada una de las subredes que corresponden con cada una de las interfaces de red presentes en nuestra máquina.

Probamos que somos capaces de llegar a nuestra puerta de enlace (gateway):

	$ ping 10.0.2.2

Para determinar la ruta que tomará el tráfico hacia un destino determinado, podemos obtenerlo con el siguiente comando:

	$ ip route get 1.1.1.1

De la salida del comando anterior, concluímos que el tráfico que vaya hacia el destino 1.1.1.1 se encaminará a través del *gateway* 10.0.2.2, con la dirección IP 10.0.2.15 como origen de dicho tráfico.

El tráfico que se dirige a alguna de nuestras direcciones IP locales, se enviará a través del *localhost*.

## IP adicional a una interfaz

Es posible que alguna de nuestras interfaces de red tenga asignada más de una dirección IP. Por ejemplo, lo conseguimos con el siguiente comando:

	$ ip address add 192.168.33.22/24 dev eth1

Y para eliminar esta nueva dirección IP, ejecutamos el siguiente comando:

	$ ip address del 192.168.33.22/24 dev eth1

## Modificar el estado de la tarjeta de red

Es posible deshabilitar una de las tarjetas de red a nivel software con el siguiente comando:

	$ ip link set eth1 down

> A pesar de que la interfaz *eth1* se encuentra deshabilitada, el tráfico con destino a la IP 192.168.33.11 logra llegar a su destino a través de la interfaz *eth0*.

## Añadir nuevas rutas

En este ejercicio, vamos a ver como se puede añadir una nueva ruta estática a mi tabla de enrutamiento para que el tráfico dirigido a una subred determinada se encamine por una de nuestras interfaces de red, y con destino hacia *centos2* en nuestro laboratorio.

	$ ip route add 172.16.0.0/12 via 192.168.33.11 dev eth1
	$ ping 172.16.0.2

## Configuración DNS

Comenzamos con la ejecución de varios comandos `ping` para comprobar la respuesta que nos envía el servicio de nombres:

	$ ping melisa.gal
	$ ping pataca.comunidadeozulo.org

Procedemos con la instalación de las herramientas `dig` y `host` para seguir haciendo prácticas con DNS:

	# yum install bind-utils -y

	$ dig melisa.gal
	[...]
	melisa.gal	232	IN	A	159.89.22.90

En la respuesta del comando anterior se comprueba que el servidor que nos envía la respuesta a esa petición tiene la IP 10.0.2.3, que corresponde con el *nameserver* configurado en nuestra máquina a través del fichero `/etc/resolv.conf`.

	$ cat /etc/resolv.conf

La consulta anterior se puede enviar directamente a un servidor de nombres concreto:

	$ dig melisa.gal @9.9.9.9

A continuación, se comprueba el estado de la configuración relativa a DNS en la propia interfaz de red:

	# nmcli device
	# nmcli -f ipv4.dns, ipv4.ignore-auto-dns connection show System\ eth0
	ipv4.dns:			--
	ipv4.ignore-auto-dns:		no

Sin embargo, si ejecutamos la misma consulta con la interfaz *eth1*, obtenemos los siguientes valores:

	# nmcli -f ipv4.dns, ipv4.ignore-auto-dns connection show System\ eth1
	ipv4.dns:			--
	ipv4.ignore-auto-dns:		yes

Podemos comprobar la configuración de dichas interfaces en los siguientes ficheros:

	$ cat /etc/sysconfig/network-scripts/ifcfg-eth[0,1]

Ahora, vamos a añadir un servidor de nombres para la interfaz *eth0*:

	# nmcli connection modify System\ eth0 ipv4.dns "9.9.9.9" ipv4.ignore-auto-dns "yes"
	# nmcli -f ipv4.dns, ipv4.ignore-auto-dns connection show System\ eth0
	ipv4.dns:			9.9.9.9
	ipv4.ignore-auto-dns:		yes


Si volvemos activar la interfaz *eth0*, conseguiremos que el servidor de nombres sea 9.9.9.9:

	# nmcli connection up System\ eth0
	# dig melisa.gal

## Primeros ejercicios con el firewall

En este momento, se ejecutan los primeros comandos para comprobar el funcionamiento del firewall, y comprobar que se bloquean las conexiones no autorizadas.

Desde la máquina *centos1* se levanta un servidor web con el siguiente comando:

	# python -m SimpleHHTPServer &

Y se comprueba que en cuanto se levanta el firewall en este máquina ya no es posible conectarse a dicho servicio desde *centos2*:

	# systemctl enable --now firewalld.service

Desde *centos2*:

	$ curl 192.168.33.10:8000
