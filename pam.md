# PAM

## Sesión 14

Para comenzar vamos a consultar los *manuales* de algunos de los módulos de PAM:

	$ man pam_unix
	$ man pam_access

En dichos manuales se encuentra el detalle de algunas de las opciones que podemos aplicar a dichos módulos. En el vídeo se explican algunas de estas opciones.

> Nos puede resultar de gran utilidad la herramienta `pamtester`, ya que permite la simulación del comportamiento de los servicios a través de la pila PAM.

El primer ejercicio consta en modificar el orden de los módulos configurados en el fichero `/etc/pam/common-auth` para que rechacen todos los intentos de iniciar sesión mediante autenticación de usuario y contraseña (por ejemplo desde el exterior con `ssh`).

Movemos la siguiente línea a la primera posición:

	auth	requisite	pam_deny.so

A partir de este momento, si intentamos conectarnos desde el exterior con el siguiente comando `ssh`, se rechazará dicha conexión:

	$ ssh -p 2222 vagrant@localhost

En el siguiente ejercicio se va bloquear el acceso para el usuario *root* a través de alguno de los terminales *tty* de nuestra máquina:

	$ man securetty
	# vi /etc/securetty

	#tty4

Después de este comentario en esa línea, el usuario *root* no podrán iniciar sesión desde la terminal *tty4*.

Vamos a trabajar con el módulo *pam_access*, y en primer lugar comprobamos en qué servicios se utiliza:

	# cd /etc/pam.d
	# grep pam_access *
	login
	sshd
	
	$ man pam_access
	$ man access.conf

Activamos la siguiente línea en el fichero `login`:

	account		required	pam_access.so

Aplicamos una restricción a través del fichero `/etc/security/access.conf`:

	-:ALL EXCEPT root:tty1

Por lo tanto, en este momento si intentamos iniciar sesión a través de *tty1* con un usuario diferente a *root* no funcionará.

Vamos a realizar un ejercicio con los requisitos establecidos para nuestras contraseñas a través del módulo `pwquality`.

	# apt install libpam-pwquality
	# grep pwquality *
	common-password...
	
	# man pam_pwquality

	# vi /etc/security/pwquality.conf

	minlen = 10
	dcredit = 1
	minclass = 2

Intentamos modificar la contraseña de *vagrant* para comprobar el efecto de los cambios anteriores.

Y para finalizar haremos un ejercicio con `libpam_mount`.

	# apt install libpam_mount
	# grep pam_mount /etc/pam.d/*
	common-auth
	common-password
	common-session

	# man pam_mount

Las configuraciones que apliquemos a este módulo deben hacerse a través del siguiente fichero:

	# vi /etc/security/pam_mount.conf.xml

Debemos indicar los volúmenes a montar entre los comentarios siguientes:

	<!-- Volume definitions -->

	<!-- pam_mount parameters: General tunables -->

Una vez explicado el funcionamiento del módulo `pam_mount`, pasaremos a revisar el módulo de gestión de límites para nuestros usuarios `pam_limits`, que se encuentra instalado por defecto en nuestro sistema.

	# grep limits /etc/pam.d/*
	[...]
	# man pam_limits

Para nuestra siguiente práctica vamos establecer un límite en el número de sesiones simultáneas a las que pueden acceder los usuarios de nuestro sistema.

	# vi /etc/security/limits.conf
	
	vagrant		hard	maxlogins	1

Para comprobar el funcionamiento intentamos iniciar sesión a través de alguna de las terminales *tty*.

Y por último vamos establecer criterios para conectarse en relación al día y/o hora permitida. Activamos la siguiente línea en el fichero `/etc/pam.d/login`:

	account		requisite		pam_time.so

	$ man pam_time
	$ man time.conf

 	# vi /etc/security/time.conf

	login;tty*;!root;!Al0000-2400
	login;tty*;vagrant;Al0000-2400


