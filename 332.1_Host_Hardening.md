# 332.1 Host Hardening

Peso: 5
Descripción: Los candidatos deben ser capaces de asegurar un equipo con Linux contra las amenazas más comunes..

## Introducción a SSH

**SSH**, *Secure SHell*, es uno de los protocolos más interesantes que tenemos a nuestra disposición para la administración de sistemas, ya que permite acceder a otros sistemas de manera remota, y además de forma segura.

Con SSH se puede utilizar un intérprete de comandos de forma remota, copiar volúmenes de datos, disponer de sesiones FTP seguras, utilización de claves RSA para fortificar la autenticación y creación de los canales seguros, crear túneles inversos o utilizar estos para fortificar otras aplicaciones mediante la potencia de este protocolo, etcétera.

## Funcionamiento del protocolo

En la imagen se puede visualizar como en primer lugar se realiza la conexión mediante TCP, realizando el *three-way handshake*, o triple apretón de manos *(SYN, SYN+ACK, ACK)*. Después de abrir la conexión, cliente y servidor se envían la versión disponible del protocolo. Actualmente, existen dos versiones del protocolo SSH, la versión 1 y la 2. La versión número 1 es totalmente desaconsejable, ya que se han encontrado fallos de seguridad. Después de enviarse la versión del protocolo disponible, se utilizará la llave pública y privada de RSA, *Rivest Shamir y Adleman*. El servidor enviará su llave pública de *host* al cliente para que este pueda cifrar lo que necesite enviar al servidor en un instante posterior. El cliente compara la llave pública de *host* con la que tenga almacendad del servidor en el fichero *known_hosts*, en la ruta *$HOME/.ssh*. Si es la primera vez que se realiza la conexión se mostrará una alerta donde se indica que no se puede verificar la identidad del servidor por parte del cliente, o si por el contrario, la clave pública es distinta a la que se tiene almacenada por el cliente se mostrará una alerta indicando que la clave ha cambiado. De este modo, se alerta al cliente que el servidor no dispone de la misma clave. La manera óptima de enviar, por primera vez, la llave pública de *host* a un cliente por parte de un servidor es vía física, aunque en la mayoría de los casos no se puede realizar de este modo, por lo que se debe confiar en el peligro. Otra opción, es identificar el *fingerprint*, o huella, para poder verificarla vía telefónica con el administrador del servidor.

Una vez que el cliente dispone de la llave pública de *host* del servidor, este generará una clave de sesión aleatoria y seleccionará un algoritmo de cifrado simétrico. Con esta clave de sesión, la cual por defecto se regenerará cada 3600 segundos, se cifrará el túnel. El cliente enviará un mensaje conteniendo la clave de sesión y el algoritmo de cifrado seleccionado, esta información viajará cifrada con la clave pública de *host* al servidor usando el algoritmo RSA. En este instante, el resto de la comunicación se utilizará el algoritmo de cifrado simétrico y la clave compartida de sesión, ya que este método es más rápido para cifrar y descifrar, pero debe ser usado primero el protocolo de *Diffie-Hellman* para poder enviar de manera segura la clave compartida de sesión.

Una vez ya se encuentra el túnel seguro creado se llevará a cabo la autenticación del usuario en el servidor. Se pueden utilizar distintos métodos para llevar a cabo la autenticación.

## Vídeo 5 - Sesión SSH

### Vagrantfile

```
Vagrant.configure("2") do |config|

         config.vm.define "centos1" do |centos1|
           centos1.vm.box = "centos/7"
           centos1.vm.network "private_network", ip: "192.168.33.10", virtualbox__intnet: "interna"
           centos1.vm.hostname = "centos1"
         end

         config.vm.define "centos2" do |centos2|
           centos2.vm.box = "centos/7"
           centos2.vm.network "private_network", ip: "192.168.33.11", virtualbox__intnet: "interna"
           centos2.vm.hostname = "centos2"
         end

         config.vm.define "centos3" do |centos3|
           centos3.vm.box = "centos/7"
           centos3.vm.network "private_network", ip: "192.168.33.12", virtualbox__intnet: "interna"
           centos3.vm.hostname = "centos3"
         end
end
```

### Primeras pruebas de conexión

Todas las pruebas que se ejecuten a continuación se pueden realizar desde un usuario sin privilegios, por ejemplo *vagrant*. En primer lugar vamos a intentar un inicio de sesión remota en *centos2* desde *centos1* través de *ssh*, con el siguiente comando:

	$ ssh vagrant@192.168.33.11

Una vez que aceptamos el *fingerprint* de nuestro equipo remoto, veremos un mensaje de error que nos impide iniciar sesión en el equipo remoto.

	[...]
	Permission denied (publickey,gssapi-keyex,gssapi-with-mic).

Del mensaje de error mostrado, se deduce que el método utilizado para iniciar la sesión remota no está dentro de los autorizados por el servidor.

Para poder iniciar la sesión remota es necesario disponer de un par de llaves (pública y privada) con las que poder identificarnos en *centos2*. Lo primero a ejecutar será la creación de dichas llaves en nuestra máquina *centos1*:

	$ ssh-keygen -b 4096 -C "Laboratorio LPIC3"
	[...]

Las nuevas llaves creadas, quedan guardadas en el directorio `.ssh` del usuario *vagrant*, con los nombres `id_rsa` y `id_rsa.pub`.

El siguiente paso será copiar la llave pública en el equipo remoto *centos2*, pero no lo podemos realizar con el comando `ssh-copy-id` al no poder identificarnos con el método de usuario y contraseña.

Por lo tanto, vamos a modificar la configuración del servicio *sshd* en *centos2* para que permita las autenticaciones basadas en *password*. Debemos editar el fichero `/etc/ssh/sshd_config`, y modificar la siguiente directiva:

	PasswordAuthentication yes

Si volvemos a ejecutar el comando para copiar la llave pública desde *centos1* debe solicitarnos la contraseña del usuario *vagrant* en el equipo remoto *centos2*.

A partir de este momento ya nos podremos conectar usando las llaves recién creadas:

	$ ssh 192.168.33.11

A continuación, vamos a generar un nuevo par de llaves en *centos1*, pero en esta ocasión usamos un algoritmo diferente al anterior:

	$ ssh-keygen -t ed25519 -C "Llave para Centos2"

Para enviar la llave pública al equipo *centos2*, ejecutamos el siguiente comando:

	$ ssh-copy-id 192.168.33.11

Y como se puede observar en la salida, se va instalar una copia de la nueva llave usando como método de autentiación la llave anterior.

Aunque, también podemos indicar expresamente la llave a enviar:

	$ ssh-copy-id -i .ssh/id_ed25519.pub 192.168.33.11

Si deseamos visualizar los pasos intermedios que se realizan durante la fase de autenticación con nuestro equipo remoto, debemos usar la opción `-v`:

	$ ssh -v 192.168.33.11 -i .ssh/id_ed25519

Ahora, nos movemos hacia la máquina *centos2* para comprobar que las llaves públicas del usuario *vagrant* se guardan en el fichero `authorized_keys` dentro del directorio `.ssh`. Aunque los nombres de los usuarios coinciden entre las dos máquinas, se trata de usuarios diferentes.

Si deseamos inciar la sesión remota con otro usuario diferente, y que puede no existir en el equipo cliente, lo indicamos con la opción `-l`:

	$ ssh centos2 -l <usuario>

Podemos ejecutar el cliente *ssh* indicando la dirección IPv6 de *centos2* del siguiente modo:

	$ ssh fe80....7ec3%eth1			# Debemos indicar la interfaz de red

> Si nuestra conexión se pierde, y no tenemos acceso a la consola remota, antes de cerrar nuestra terminal cliente, podemos ejecutar los siguientes caracteres `~.` y podremos recuperar la sesión en el equipo cliente.

## Fichero de configuración del equipo cliente

Desde nuestra máquina *centos1* vamos a crear un fichero de configuración para el usuario *vagrant* que nos permita adaptar el comand *ssh* a nuestras necesidades:

	$ > .ssh/config
	$ chmod 600 .ssh/config

Y dentro de este nuevo fichero insertamos el siguiente contenido:

	Host Centos2
	  Hostname 192.168.33.11
	  User vagrant

Ahora, podemos ejecutar el siguiente comando:

	$ ssh Centos2

Vamos ampliar la configuración para ese nodo *Centos2* para hacer uso de la llave recién creada en lugar de las credenciales. Añadimos la siguiente línea a la sección anterior:

	IdentityFile ~/.ssh/id_ed25519

	$ ssh -v Centos2

Si deseamos aplicar una configuración global para todos nuestros servidores, lo haremos con las siguientes líneas (en el ejemplo hacemos uso de un usuario por defecto):

	Host *
	  User <usuario>

Incluso indicando un subdominio:

	Host *.example.com
	  Port 2200

## Configuración del servidor

Se van realizar cambios en la configuración del servidor, y comenzamos con el cambio del puerto en el que se esperan las conexiones de los clientes. No se debe modificar la configuración por defecto, que es la de la línea `Port 22`, y lo que se hace es añadir la siguiente:

	ListenAddress 192.168.33.11:2200

	$ sudo systemct	restart sshd

Al intentar reiniciar el servicio, se produce un error, por lo que tendremos que intentar localizar el motivo a través de la observación del siguiente fichero de *log*:

	$ sudo tail -f /var/log/audit/audit.log

El error está provocado por la protección de SELinux, al intentar conectar un servicio a un puerto para el que no tiene permiso. La solución está descrita en el propio fichero de configuración:

	$ sudo yum install policycoreutils-python
	$ sudo semanage port -a -t ssh_port_t -p tcp 2200
	$ sudo systemctl restart sshd

Por lo tanto, para conectarme desde la máquina *centos1* se debe cambiar la configuración en el fichero `.config`, indicando el puerto 2200.

Para aumentar el nivel de información que nos proporciona el servicio modificamos la siguiente línea en el fichero de configuración del servicio:

	LogLevel VERBOSE

Impedimos que el usuario *root* se puede conectar a través de este servicio:

	PermitRootLogin no

Activamos la visualización del mensaje del día para las nuevas conexiones:

	PrintMotd yes

El mensaje se almacena en el fichero `/etc/motd`. Algo similar podemos conseguir con la opción `Banner`.

Y reiniciamos el servicio:

	# systemctl restart sshd

Revisar en la documentación el propósito de la opción `UseDNS no`, y en que casos se puede aplicar.

Se puede establecer una lista de usuarios autorizados, a través de la opción `AllowUsers`:

	AllowUsers <usuarios>

### Fingerprint

Cuando realizamos la primera conexión a nuestro servidor, debemos aceptar el valor del *fingerprint*, que se guarda en el fichero local `~/.ssh/known_hosts`. Este valor está guardado en el servidor, en concreto en el directorio `/etc/ssh/`. Por ejemplo, en el fichero `ssh_host_ecdsa_key.pub`.

Si deseamos comprobar que ambos *fingerprint* son iguales, podemos hacer uso de la versión en formato ASCII, con el siguiente comando:

	ssh-keygen -lv -f /etc/ssh/ssh_host_ecdsa_key.pub		# en el servidor
	ssh-keygen -lv -f ~/.ssh/known_hosts				# en el cliente

A continuación se van a cambiar los valores de las llaves de *host* en la parte del servidor y ver que efecto provocan en las conexiones realizadas desde el cliente.

	# Comandos en el servidor
	
	$ sudo mv /etc/ssh/ssh_host_ecdsa_key* ~

Si ahora ejecutamos una nueva conexión desde el cliente, aparece un nuevo *fingerprint* que debemos aceptar para finalizar la conexión. En realidad, lo que estamos haciendo es aceptar otra llave tipo *host* que fue creada con un algoritmo diferente (en lugar de ECDSA, ahora usaremos ED25519, por ejemplo.

Ahora, tendremos que generar unas nuevas llaves de tipo *host* en el servidor, para que el cliente muestre una advertencia de que no se confía en la información enviada desde el servidor, al no coincidir con la que tenemos guardada. Para aceptar la nueva llave, debemos eliminar antes la versión anterior con el siguiente comando:

	$ ssh-keygen -R 192.168.33.11

## Forwarding

Para realizar esta práctica levantamos un servidor web en *centos2* con el siguiente comando:

	$ python -m SimpleHTTPServer 8888

Y ahora creamos la conexión segura desde el equipo cliente *centos1*:

	$ ssh -p2200 -f -L 9999:127.0.0.1:8888 192.168.33.11 sleep 60
	$ curl 127.0.0.1:9999

Ahora, creamos esta configuración haciendo uso del fichero de configuración de *ssh*:

	Host CentOS2
	  ...
	  LocalForward 9999 127.0.0.1:8888

	$ ssh -f CentOS2 sleep 60
	$ curl 127.0.0.1:9999

Se plantea el siguiente ejercicio con 3 máquinas. En la máquina *centos2* debe estar configurada una interfaz de red para poder llegar a *centos3* que está en un segmento de red diferente a *centos1*, por lo que la conexión directa entre ambos extremos no es posible. Lo que se pretende es que *centos2* sirva de pasarela entre las dos máquinas, *centos1* y *centos3*. Para hacer las pruebas, podemos levantar un servidor web en *centos3*, e intentar realizar una petición *http* desde *centos1*.

A continuación, veremos el concepto de *RemoteForwarding*, en contraposición con el anterior.

En esta ocasión, levantamos el servidor web en el equipo cliente, *centos1*, y ejecutamos el siguiente comando para realizar el túnel:

	$ ssh -p2200 -R 7777:127.0.0.1:8888 192.168.33.11

Ahora, si nos movemos al servidor *centos2* y ejecutamos el siguiente comando, obtendremos una respuesta por parte del servidor web:

	$ curl 127.0.0.1:7777

De nuevo, veremos como aplicar esta configuración en el fichero de *ssh*:

	Host CentOS2
	  ...
	  RemoteForward 7777 127.0.0.1:8888

## Agente SSH

Cuando ejecutamos este agente, lo que hace es generar un par de variables de sistema en las cuales guarda el PID del propio agente (SSHi\_AGENT\_PID), y el socket (SSH\_AUTH\_SOCK).

	$ ssh-agent

La salida que muestra el comando anterior, en realidad corresponde con los comandos que debemos ejecutar desde la línea de comandos para poder declarar las variables que se comentaban más arriba.

	SSH_AUTH_SOCK=/tmp ....; export SSH_AUTH_SOCK;
	SSH_AGENT_PID=5858; export SSH_AGENT_PID;

Una vez que tenemos el agente en memoria, y las variables declaradas, debemos añadir nuestra llave privada a dicho agente:	

	$ ssh-add -l		# Visualiza las llaves cargadas
	$ ssh-add .ssh/id_ed25519

En este momento, nos podemos conectar a nuestro equipo remoto de forma automática, sin tener que introducir la contraseña de la llave privada. Si deseamos eliminar la llave privada de nuestro agente, debemos hacer uso de la opción `-d`.

El bloqueo del agente impide la consulta de las llaves cargadas, así como las operaciones de añadido de nuevas llaves o de borrado de las existentes.

	$ ssh-add -x		# Bloquea
	$ ssh-add -X 		# Desbloquea

Añadir una llave al agente haciendo uso del fichero de configuración de SSH. Debemos añadir la siguiente opción a una de las secciones *Host* del fichero *config*:

	AddKeysToAgent yes

De este modo, cuando ejecute la primera conexión a mi equipo remoto se añadirá de forma automática dicha llave al agente, con lo que en próximas conexiones esa llave ya estará cargada en el agente.

Queda como tarea la revisión de alguna de las opciones que aparecen descritas en el manual de `ssh_config`, y ver si alguna de ellas nos puede resultar útil, bien ahora o en algún momento futuro.


