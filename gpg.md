# GPG

En la primera parte de la sesión número 10 se hace una breve introducción a `packer`, como herramienta para la creación de *boxes* que serán utilizadas posteriormente por Vagrant.

A partir del minuto 6:00 comienzan los contenidos de GPG.

Para este laboratorio se usa dos máquinas virtuales CentOS 7, haciendo uso de un fichero Vagrantfile como el siguiente:

```
Vagrant.configure("2") do |config|

         config.vm.define "centos1" do |centos1|
           centos1.vm.box = "centos/7"
           centos1.vm.network "private_network", ip: "192.168.33.10", virtualbox__intnet: "interna"
           centos1.vm.hostname = "centos1"
         end

         config.vm.define "centos2" do |centos2|
           centos2.vm.box = "centos/7"
           centos2.vm.network "private_network", ip: "192.168.33.11", virtualbox__intnet: "interna"
           centos2.vm.hostname = "centos2"
         end
end

```

El objetivo es que estas dos máquinas puedan simular un entorno en el cual se transmite información cifrada entre ellos.

Una vez que tengo levantadas las dos máquinas, actualizamos el fichero `/etc/hosts` para que las dos máquinas puedan conocer la dirección IP y el *hostname* entre ellas.

Creamos dos usuarios para usar en los ejercicios; en *centos1* creamos al usuario *bob*, y en *centos2* a *alice*.

Comenzamos con la generación del par de llaves para nuestros usuarios anteriores.

	$ gpg --gen-key
	[...]
	Your selection? 1
	[...]
	What keysize do you want? (2048) <intro>
	[...]
	Key is valid for? (0) 30
	Is this correct? (y/N) y
	[...]
	Real name: <nombre>
	Email address: <email>
	Comment: <comentario>
	Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit? O
	[...]
	

> Tal y como se explica en la sesión, a la hora de generar las llaves en una sesión iniciada con el comando `su` se presenta un fallo a la hora de introducir la *llave de paso*, por lo que es necesario iniciar una sesión desde el propio comando *ssh* en el equip anfitrión.

Antes debemos habilitar el acceso con contraseña:

	$ vagrant ssh centos1
	
	$ sudo vi /etc/ssh/sshd_config
	PasswordAuthentication yes
	$ sudo systemctl restart sshd
	$ exit

	$ ssh -p 2222 bob@localhost

Y llegados a este punto, volvemos a ejecutar el comando para la creación de las llaves.

	$ gpg --gen-key

En este punto nos encontramos con la ausencia de entropía suficiente para la generación de las llaves. La solución pasa por la instalación de la utilidad `rng-tools`:

	# yum install rng-tools -y
	# rngd

A partir de este momento ya contamos con la entropía suficiente para la generación de las llaves.

> En otras distribuciones podemos hacer uso de la utilidad `haveged`. En CentOS debemos usar el repositorio *epel* para poder instalarla.

Para visualizar las llaves disponibles:

	$ gpg --list-keys

Ahora tendremos que generar las llaves para *alice* en la máquina *centos2* siguiendo los mismos pasos que en *centos1*.

## Cifrado simétrico

En este tipo de cifrado se va a compartir la *frase de paso* entre los usuarios. Es decir, cuando se cifre el fichero se usará una *frase de paso* (que se usará para este cifrado en concreto, y que debe elegir el remitente) que deberá conocer el destinatario para poder acceder al contenido de dicho fichero.

Ejecutamos los siguientes comandos en *centos2*:

	$ cp /etc/passwd passwd
	$ gpg -c passwd
		Passphrase
	$ ls -l
	passwd.gpg

	$ scp passwd.gpg bob@centos1:

Nos cambiamos a *centos1* para intentar abrir el fichero enviado.

	$ less passwd.gpg
		Passphrase

Para descifrar el fichero, ejecutamos el siguiente comando:

	$ gpg -d passwd.gpg
	
Una medida de seguridad adicional que podemos aplicar en la máquina donde se ha cifrado el fichero, es la eliminación del original con el siguiente comando:

	$ shred -u -z passwd

En el siguiente ejercicio vamos hacer del comando `tar` para crear un archivo de varios ficheros.

	$ touch file{1..10}.txt
	$ sudo mkdir /backup
	$ sudo chown bob: /backup
	$ sudo chmod 700 /backup

El archivo creado con `tar` se enviará al comando `gpg` para que ejecute el cifrado y el resultado será almacenado en un fichero.

	$ tar cJvf - /home/bob/ | gpg -c > /backup/bob_backup.tar.xz.gpg

> Aunque se detalla en la siguiente sesión, para poder ejecutar correctamente el anterior comando debemos declarar la variable `GPG\_TTY`, con el siguiente comando: `export GPG_TTY=$(tty)`.

## Cifrado asimétrico

Ahora *Bob* y *Alice* van a compartir sus llaves públicas. Ejecutamos el siguiente comando desde *centos1*:

	$ gpg --export -a -o bob-pubkey.txt
	$ less bob-pubkey.txt
	$ scp bob-pubkey.txt alice@centos2:

Y en *centos2*:

	$ gpg --export -a -o alice-pubkey.txt
	$ scp alice-pubkey.txt bob@centos1:

Realizamos la importación de las llaves foráneas en cada una de las máquinas:

	$ gpg --import <usuario>-pubkey.txt

Ahora, desde la máquina *centos1* enviaremos un fichero cifrado con la llave pública de *alice*.

	$ cat > mensaje.txt
	Esto es un mensaje de alto secreto...
	$ gpg -s -e mensaje.txt

Nos pide la contraseña para acceder a la llave privada del usuario local (*bob*). Y luego tendremos que introducir el *user ID* del destinatario del mensaje. Una vez finalizada la ejecución del comando, tendremos generado un fichero con el nombre `mensaje.txt.gpg`.

	$ scp mensaje.txt.gpg alice@centos2:

Desde el equipo destinatario del mensaje (centos2), ejecutamos el siguiente comando para descifrar el fichero:

	$ less mensaje.txt.gpg

Nos pide la contraseña para acceder a la llave privada de *alice*, y podremos acceder al contenido del fichero original.

## Nivel de confianza

Vamos elevar el nivel de confianza en la llave importada para el usuario *alice* en la máquina *centos1*:

	$ cd .gnupg/
	$ gpg -edit-key Alice
	[...]
	gpg> trust
	[...]
	Your decision? 5

A partir de este momento podemos cifrar ficheros con la llave pública de *alice* sin que se muestre ningún mensaje de advertencia respecto a la confianza en dicha llave.

## Sesión 11 - GPG con correo electrónico

El primer ejercicio que se realiza en este vídeo corresponde a la práctica que quedara pendiente en la sesión anterior, donde se pretendía cifrar un archivo comprimido con el comando `tar`. Para ello, es necesario declarar la variable `GPG_TTY`, y así poder introducir la *frase de paso* que nos solicita el comando `gpg`:

	$ tar cJvf - /home/bob/ | gpg -c > /tmp/bob-backup.tar.xz.gpg
	$ scp /tmp/bob-backup.tar.xz.gpg  alice@centos2:

Y, desde la máquina *centos2*:

	$ gpg -d bob-backup.tar.xz.gpg | tar xvJ

### Servidor de correo electrónico

Configuraremos la máquina *centos1* como servidor de correo para nuestra red local. Para ello, también será necesario disponer de un servidor de nombres (DNS) en la misma máquina.

Creamos usuarios adicionales para realizar las siguientes prácticas:
	
	# adduser linus
	# passwd linus

Instalamos el servidor de nombres, en este caso usaremos *dnsmasq*.

	# yum install dnsmasq -y
	# vi /etc/hosts
	192.168.33.11	centos2
	
Cambiamos el *hostname* de *centos1*, y actualizamos los valores en el fichero `/etc/hosts`:

	# hostname mail
	# vi /etc/hosts
	127.0.0.1	mail mail.lpic.lan

A continuación, configuramos el servicio *postfix*:

	# vi /etc/postfix/main.cf
	
	myhostname = mail.lpic.lan
	mydomain = lpic.lan
	myorigin = $mydomain
	inet_interfaces = all
	mydestination = $myhostname, localhost.$mydomain, localhost, $mydomain
	mynetworks = 192.168.33.0/24, 127.0.0.0/8
	home_mailbox = Maildir/
	
	# systemctl restart postfix

Instalamos un cliente de correo para poder usar desde la terminal:

	# yum install mailx -y

Realizamos la prueba de envío de un mensaje a nuestro usuario local *linus*:

	$ echo "Mensaje" | mail -s "Probando postfix" linus@lpic.lan

Si queremos recuperar el mensaje enviado a *linus* debemos realizar los siguientes cambios desde la sesión de dicho usuario:

	$ export MAIL=~/Maildir
	$ mail
	[...]

Después de analizar la posible solución a la declaración de la variable *MAIL* a nivel de sistema, vamos a realizar un envío de un mensaje cifrado desde el usuario *linus*.

	$ export GPG_TTY=$(tty)
	$ echo "Mensaje secreto" | gpg -c | mail -s "Secreto" vagrant@lpic.lan

> Aunque en el vídeo no funciona correctamente cuando lo ejecutamos desde el usuario *linus*, es importante comprobar el estado de la variable *GPG_TTY* para evitar posibles fallos a la hora de introducir la *frase de paso*.

Queda pendiente de visualizar el mensaje en claro en el lado del destinatario haciendo uso del comando `mail`.

Ahora, nos vamos a la máquina *centos2* para que se puedan enviar mensajes de correo electrónico. En primer lugar, cambiamos la dirección IP del servidor DNS configurado en `/etc/resolv.conf`:

	# vi /etc/resolv.conf
	nameserver 192.168.33.10
	search lpic.lan

Después de realizar estos cambios, observamos que cuando ejecutamos un `ping` a `mail.lpic.lan` nos responde `127.0.0.1`, y esto es debido a que es la información que está guardada en el fichero `/etc/hosts` del servidor de nombres en *centos1*.

Para tener disponibles comandos como `dig`, vamos a instalar el paquete `bind-utils` en *centos2*:

	# yum install bind-utils -y

Volvemos a *centos1* para añadir las siguientes líneas al fichero de configuración de *dnsmasq*:

	# vi /etc/dnsmasq.conf

	auth-zone=lpic.lan
	auth-soa=123456,root.lpic.lan
	mx-host=lpic.lan,mail.lpic.lan,10
	
Con esta configuración, si intentamos enviar un mensaje desde *centos2* se intentará entregar en la IP `127.0.0.1` que es la que tiene configurada el servidor de nombres para `mail.lpic.lan`. Por lo que ahora tendremos que solucionar esta incidencia.

Cambiamos el siguiente valor en el fichero `/etc/hosts` de *centos1*:

	192.168.33.10	mail mail.lpic.lan

Probamos a enviar un nuevo mensaje desde *centos2* al usuario *linus*:

	$ echo "Hola desde centos2" | mail -s "Hola" linus@lpic.lan

Y podemos comprobar que se ha recibido correctamente por parte del destinatario.

Para poder mejorar las prestaciones de nuestro servidor de correo, vamos a instalar `dovecot`, y así poder disponer de los protocolos *POP* e *IMAP* en esa máquina.

	# yum install dovecot -y

	# vi /etc/dovecot/dovecot.conf
 	
	protocols = imap pop3 lmtp
	listen = *

	# vi /etc/dovecot/conf.d/10-auth.conf

	disable_plaintext_auth = no
	auth_mechanisms = plain login

	# vi /etc/dovecot/conf.d/10-mail.conf

	mail_location = maildir:~/Maildir
 
	# vi /etc/dovecot/conf.d/10-master.conf

	unix_listener /var/spool/postfix/private/auth {
	  mode = 0666
	  user = postfix
	  group = postfix
	}
 
	# vi /etc/dovecot/conf.d/10-ssl.conf

	ssl = no

	# systemctl start dovecot

Y ahora ajustamos la configuración de *postfix*:

	# vi /etc/postfix/main.cf

	smtpd_sasl_type = dovecot
	smtpd_sasl_path = private/auth
	smtpd_sasl_auth_enable = yes
	smtpd_sasl_security_options = noanonymous
	smtpd_sasl_local_domain = $myhostname
	smtpd_recipient_restrictions = permit_mynetworks, permit_auth_destination, permit_sasl_authenticated, reject 

	# systemctl restart postfix

Para continuar con este laboratorio, se crearán las llaves *gpg* para el usuario *linus* y se envía la llave pública al usuario *alice* en *centos2*, con la idea de que pueda enviar mensajes cifrados a *linus*. Cuando se reciba en *centos2*, el usuario *alice* debe importar dicha llave para poder enviar los mensajes cifrado a *linus*.

	$ gpg --import linus-pubkey.txt
	$ echo "Mensaje" | gpg -e -r linus@lpic.lan | mailx -s "Prueba de Alice" linus@lpic.lan
	[...]

Se añade una máquina virtual Windows a nuestro laboratorio en la que poder instalar un gestor de correo Thunderbird para poder gestionar el correo del usuario *linus*. Se propone la descarga de una máquina virtual de prueba desde la siguinte URL: https://developer.microsoft.com/en-us/microsoft-edge/tools/vms/

## Sesión 12 - Correo electrónico cifrado con GPG

Continuamos con el laboratorio utilizado hasta el momento. En el que se añade una máquina adicional (centos3) replicando la configuración existente en el fichero `Vagrantfile`. En esta máquina se va instalar un gestor gráfico para poder realizar la instalación posterior de un cliente de correo electrónico. Ejecutamos el siguiente comando para instalar `xfce` en *centos3*:

	# yum -y groups install "Server with GUI"
	# yum install epel-release -y
	# yum -y groups install "Xfce"
	# systemctl set-default graphical.target

Añadimos las siguientes líneas en el bloque de *centos3* dentro del fichero `Vagrantfile`:

	config.vm.provider "virtualbox" do |v|
	  v.gui = true
	end

Instalamos un cliente de correo para *Xfce*:

	# yum install trojita -y

También se hace uso de la máquina virtual de Windows 7/10 descargada en la sesión anterior. Hay que añadir una interfaz de red adicional para que se pueda conectar a nuestra red *interna* del laboratorio, y poder conectarse al servidor de correo en *centos1*. Le asignamos la dirección IP `192.168.33.20`, e instalamos Thunderbird. Una vez instalado el gestor de correo, Thunderbird, configuramos la cuenta de correo para el usuario *linus*, indicando como servidor IMAP y SMTP la dirección IP de *centos1* (192.168.33.10).

Vamos a enviar un correo cifrado desde el usuario *alice* de *centos2*:

	$ echo "algo secreto" | gpg -ca | mailx -s "sesion de lpic" linus@lpic.lan

> Con la opción `-a` del comando `gpg` hacemos que la salida cifrada se codifique con caracteres ASCII

Una vez enviado el mensaje con la codificación ASCII, se puede recuperar en el destino con el comando `gpg` indicando el fichero del directorio `Maildir`:

	$ gpg -d ~/Maildir/<fichero>

Para poder visualizar el mensaje cifrado desde la máquina con Windows, debemos instalar el complemento *Enigmail* en Thunderbird, que también incorpora la utilidad *GnuPG*.

Para el siguiente ejercicio se va hacer uso del comando `cowsay` para enviar una de sus salidas como cuerpo del mensaje a cifrar. También lo enviamos desde *centos2*:

	$ echo "Hola Linus" | cowsay | gpg -ear linus@lpic.lan | mailx -s "Mensaje de la vaca" linus@lpic.lan

Cuando se reciba este mensaje en la cuenta del usuario *linus* no podremos abrirlo al no disponer todavía de la llave privada de este usuario en el cliente Thunerbird de la máquina Windows, por lo que será necesario realizar su importación desde *centos1*. Para poder realizar este proceso vamos a habilitar un servicio NFS en *centos1* al cual accederá la máquina Windows.

Ejecutamos estes comandos en *centos1*:

	# mkdir /public
	# chmod 777 /public
	# vi /etc/exports

	/public *(ro)

	# exportfs -r

> Atención al estado del servicio *rpcbind* ya que será necesario activarlo.

Desde la máquina Windows se debe habilitar la característica del sistema *Sevices for NFS* para poder acceder al recurso NFS de *centos1*. Y ejecutamos el siguiente comando desde el *símbolo del sistema*:

	> mount 192.168.33.10:/public Z:

Desde el equipo *centos1* exportamos la llave privada del usuario *linus*:

	$ gpg --export-secret-keys --armor --output /public/linus-privkey.asc

Desde la máquina Windows será suficiente con hacer un doble clic sobre el fichero anterior para que se pueda importar esta llave privada del usuario *linus*.

### Certificado de revocación

Es aconsejable disponer de un certificado de revocación de nuestras llaves para poder utilizar en caso de necesidad, por ejemplo, que se vea comprometida la seguridad de nuestra llave.

	$ gpg --gen-revoke linus@lpic.lan

### Material adicional - Autenticación SSH en dos pasos

Referencia: https://github.com/google/google-authenticator

> **NOTA** Al final de la sesión 12 se intenta realizar la instalación y configuración en una distribución CentOS que no ha funcionado correctamente. Por lo tanto, al inicio de la sesión 13 se realiza dicha configuración en una distribución Ubuntu *bionic*.

En la siguiente práctica se va implementar un segundo factor de autenticación para poder conectarnos a nuestra máquina *centos1* a través de SSH. La idea es que a mayores de la contraseña que hemos usado hasta el momento, el sistema nos pida uno de los códigos generados por la aplicación *Google Authenticator* que hemos instalado en nuestro dispositivo móvil.

Instalamos el paquete `libpam-google-authenticator`:

	# apt install libpam-google-authenticator

Añadimos la siguiente línea al fichero `/etc/pam.d/sshd` (justo después de la línea `@include common-auth`):

	auth	required 	pam_google_authenticator.so [debug]

> De forma opcional, podemos añadir la opción *debug* a este módulo para poder analizar su actividad.
> Al activar este módulo como `required`, debo autenticarme correctamente para iniciar la sesión. Sin embargo, la opción `sufficient` hace que sea suficiente la autenticación con este módulo, y si no es posible, sigue hacia abajo ignorado el resultado de la misma. Este comportamiento se explica en el ejercicio disponible en el vídeo de la sesión.

Modificamos las siguientes líneas en el fichero `/etc/ssh/sshd_config`:

	PasswordAuthentication yes
	ChallengeResponseAuthentication yes

	# systemctl restart sshd

Generamos el token en nuestro servidor (usuario *vagrant*) con la ejecución del comando `google-authenticator`, y lo integramos con la *app* del móvil.

	$ google-authenticator

Ahora, desde la máquina anfitrión ejecuto el siguiente comando para iniciar sesión a través de *ssh*:

	$ ssh -p 2222 vagrant@localhost
	Password:
	Verification code:

> Tal y como se detalla al inicio de la sesión 13, la causa del error en la autenticación con el segundo factor del vídeo anterior, estaba en que se había intentado el acceso con un usuario diferente al que se había usado para generar la clave secreta. En concreto, el comando de generación de las claves dentro de la máquina se había ejecutado con el usuario *vagrant*, y la conexión exterior a través de *ssh* se había intentado realizar con el usuario *alice* (LOL).

Una vez solucionado el error anterior, se procede a realizar la misma instalación y configuración pero en una distribución Ubuntu bionic. Lo único que se debe tener en cuenta es la diferente localización de ciertos ficheros de configuración de *PAM*.

Una vez instalado el paquete:

	# apt update
	# apt install libpam-google-authenticator

Debemos realizar algunos ajustes en el fichero de configuración del servicio *ssh* en PAM:

	# vi /etc/pam.d/sshd

Justo a continuación de la cuarta línea (`@include common-auth`), añadimos la siguiente:

	auth	required	pam_google_authenticator.so

**NOTA** Es muy importante realizar los siguientes cambios en el servicio *ssh*, sobre todo la directiva `ChallengeResponseAuthentication` a `yes`, ya que es la opción que permite el *prompt* para solicitar el *Verification code*.

> Debemos habilitar el inicio de sesión con *password* en el servicio *ssh*. Y habilitar la opción `ChallengeResponseAuthentication yes`.
> Para evitar problemas futuros, es aconsejable actualizar la contraseña del usuario *vagrant*, ya que la tendremos que usar en las siguientes pruebas.

Ahora, desde la sesión del usuario *vagrant* se procede a generar la llave secreta con el comando `google-authenticator`, y escaneamos el código QR con la aplicación del móvil.

Y desde el exterior probamos a conectarnos a través de *ssh* con el siguiente comando:

	$ ssh -p 2222 vagrant@localhost
	vagrant@localhost's password:	

> Aunque en la primera parte del vídeo vuelve a fallar la autenticación, en esta ocasión la causa está en la variable `ChallengeResponseAuthentication`.


